package com.advansoft.assignment.constant;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public class Constant {
    public static final String PENDING = "Pending";
    public static final String DONE = "Done";
    public static final String GROCERY = "GROCERY";
    public static final String GROCERY_UNIT = "kg";
    public static final String GARMENTS = "GARMENTS";
    public static final String GARMENTS_UNIT = "size";
    public static final String COOKING = "COOKING";
    public static final String COOKING_UNIT = "kg";
}
