package com.advansoft.assignment.dagger;

import android.app.Application;

/**
 * Created by Asif Bakht on 11/9/2017.
 */

public class Advantsoft extends Application {
    private AppComponents appComponents;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponents = initAppComponents(this);
    }

    private AppComponents initAppComponents(Advantsoft app) {
        return DaggerAppComponents.builder()
                .appModule(new AppModule(app))
                .dataSourceModule(new DataSourceModule())
                .repositoryModule(new RepositoryModule())
                .build();
    }

    public AppComponents getAppComponents() {
        return appComponents;
    }
}
