package com.advansoft.assignment.dagger;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Asif Bakht on 11/9/2017.
 */

@Module
public class AppModule {

    private Advantsoft application;

    public AppModule(Advantsoft application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return this.application;
    }
}
