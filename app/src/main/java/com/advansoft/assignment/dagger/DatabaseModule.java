package com.advansoft.assignment.dagger;

import android.content.Context;

import com.advansoft.assignment.data.catalog.ProductDbHelper;
import com.advansoft.assignment.data.shoppingcart.source.ShoppingCartDbHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

@Module
public class DatabaseModule {

    @Provides
    @Singleton
    public ProductDbHelper providesProductDbHelper(Context context) {
        return new ProductDbHelper(context);
    }

    @Provides
    @Singleton
    public ShoppingCartDbHelper providesShoppingCartDbHelper(Context context) {
        return new ShoppingCartDbHelper(context);
    }
}