package com.advansoft.assignment.dagger;

import android.content.Context;

import com.advansoft.assignment.data.catalog.RepositoryProduct;
import com.advansoft.assignment.data.shoppingcart.source.RepositoryShoppingCart;
import com.advansoft.assignment.ui.cart.activity.ShoppingCartActivity;
import com.advansoft.assignment.ui.catalog.activity.CatalogActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Asif Bakht on 11/9/2017.
 */

@Singleton
@Component(modules = {AppModule.class, DataSourceModule.class, DatabaseModule.class, RepositoryModule.class})
public interface AppComponents {

    void inject(CatalogActivity catalogActivity);

    void inject(ShoppingCartActivity shoppingCartActivity);

    Context getContext();

    RepositoryProduct getRepositoryProduct();

    RepositoryShoppingCart getRepositoryShoppingCart();

}
