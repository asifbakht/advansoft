package com.advansoft.assignment.dagger;

import com.advansoft.assignment.data.catalog.RepositoryProduct;
import com.advansoft.assignment.data.catalog.source.local.LocalDataSourceProduct;
import com.advansoft.assignment.data.catalog.source.remote.RemoteDataSourceProduct;
import com.advansoft.assignment.data.shoppingcart.source.RepositoryShoppingCart;
import com.advansoft.assignment.data.shoppingcart.source.local.LocalDataSourceShoppingCart;
import com.advansoft.assignment.data.shoppingcart.source.remote.RemoteDataSourceShoppingCart;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

@Module
public class RepositoryModule {

    @Provides
    @Singleton
    public RepositoryProduct provideRepositoryProduct(LocalDataSourceProduct localSource,
                                                      RemoteDataSourceProduct remoteSource) {
        return RepositoryProduct.getInstance(localSource, remoteSource);
    }

    @Provides
    @Singleton
    public RepositoryShoppingCart provideRepositoryShoppingCart(LocalDataSourceShoppingCart localSource,
                                                                RemoteDataSourceShoppingCart remoteSource) {
        return RepositoryShoppingCart.getInstance(localSource, remoteSource);
    }
}
