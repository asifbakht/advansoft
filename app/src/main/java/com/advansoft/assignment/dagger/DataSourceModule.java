package com.advansoft.assignment.dagger;

import com.advansoft.assignment.data.catalog.ProductDbHelper;
import com.advansoft.assignment.data.catalog.source.local.LocalDataSourceProduct;
import com.advansoft.assignment.data.catalog.source.remote.RemoteDataSourceProduct;
import com.advansoft.assignment.data.shoppingcart.source.ShoppingCartDbHelper;
import com.advansoft.assignment.data.shoppingcart.source.local.LocalDataSourceShoppingCart;
import com.advansoft.assignment.data.shoppingcart.source.remote.RemoteDataSourceShoppingCart;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

@Module
public class DataSourceModule {

    @Provides
    @Singleton
    public RemoteDataSourceProduct providesRemoteDataSourceProduct() {
        return RemoteDataSourceProduct.getInstance();
    }

    @Provides
    @Singleton
    public LocalDataSourceProduct provideLocalDataSourceProduct(ProductDbHelper productDbHelper) {
        return LocalDataSourceProduct.getInstance(productDbHelper);
    }


    @Provides
    @Singleton
    public RemoteDataSourceShoppingCart providesRemoteDataSourceShoppingCart() {
        return RemoteDataSourceShoppingCart.getInstance();
    }

    @Provides
    @Singleton
    public LocalDataSourceShoppingCart provideLocalDataSourceShoppingCart(ShoppingCartDbHelper shoppingCartDbHelper) {
        return LocalDataSourceShoppingCart.getInstance(shoppingCartDbHelper);
    }


}
