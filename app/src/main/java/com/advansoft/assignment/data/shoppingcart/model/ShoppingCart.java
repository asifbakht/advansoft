package com.advansoft.assignment.data.shoppingcart.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.advansoft.assignment.data.catalog.model.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public class ShoppingCart implements Parcelable {
    List<OrderItem> items;

    public ShoppingCart() {
        items = new ArrayList();
    }

    public boolean contains(Product product) {
        return false;
    }

    public void addItem(Product product, int quantity) {
        OrderItem item = new OrderItem();
        item.setProduct(product);
        item.setQuantity(quantity);
        items.add(item);
    }

    public int getItemSize() {
        return items.size();
    }

    public List<OrderItem> getItems() {
        return this.items;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.items);
    }

    protected ShoppingCart(Parcel in) {
        this.items = new ArrayList<OrderItem>();
        in.readList(this.items, OrderItem.class.getClassLoader());
    }

    public static final Parcelable.Creator<ShoppingCart> CREATOR = new Parcelable.Creator<ShoppingCart>() {
        @Override
        public ShoppingCart createFromParcel(Parcel source) {
            return new ShoppingCart(source);
        }

        @Override
        public ShoppingCart[] newArray(int size) {
            return new ShoppingCart[size];
        }
    };
}
