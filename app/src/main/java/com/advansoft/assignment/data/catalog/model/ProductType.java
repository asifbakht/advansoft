package com.advansoft.assignment.data.catalog.model;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public enum ProductType {

    DAIRY("Fresh Dairy"),
    COOKING("Cooking Items"),
    GARMENTS("Garments");

    public String name;

    ProductType(String name) {
        this.name = name;
    }

}
