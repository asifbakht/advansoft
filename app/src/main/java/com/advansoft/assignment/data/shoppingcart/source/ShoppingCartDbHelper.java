package com.advansoft.assignment.data.shoppingcart.source;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.advansoft.assignment.constant.Constant;
import com.advansoft.assignment.data.catalog.model.Product;
import com.advansoft.assignment.data.shoppingcart.model.OrderItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public class ShoppingCartDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "eCommerce";
    private static final int DATABASE_VERSION = 1;
    private Context context;

    /*
    TABLE name
     */

    private static final String TABLE_NAME_SHOPPING_CART = "ShoppingCart";

    /*
    TABLE columns
     */
    private static final String COLUMN_NAME_ID = "_id";
    private static final String COLUMN_NAME_NAME = "name";
    private static final String COLUMN_NAME_DESCRIPTION = "description";
    private static final String COLUMN_NAME_UNIT = "unit";
    private static final String COLUMN_NAME_PRICE = "price";
    private static final String COLUMN_NAME_IMAGE_URL = "image_url";
    private static final String COLUMN_NAME_STATUS = "status";
    private static final String COLUMN_NAME_QUANTITY = "quantity";
    private static final String COLUMN_NAME_PRODUCT_ID = "product_id";


    private static final String CREATE_TABLE_PRODUCT = String.format("CREATE TABLE %s " +
                    " (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s TEXT, " +
                    "  %s DOUBLE, %s TEXT, %s Text, %s INTEGER, %s INTEGER);", TABLE_NAME_SHOPPING_CART,
            COLUMN_NAME_ID, COLUMN_NAME_NAME, COLUMN_NAME_DESCRIPTION,
            COLUMN_NAME_UNIT, COLUMN_NAME_PRICE, COLUMN_NAME_IMAGE_URL, COLUMN_NAME_STATUS,
            COLUMN_NAME_QUANTITY, COLUMN_NAME_PRODUCT_ID);

    private static final String DROP_TABLE_SHOPPING_CART = String.format("DROP TABLE IF EXISTS %s", TABLE_NAME_SHOPPING_CART);

    String columnWhereClauseById = COLUMN_NAME_ID + " = ? ";


    public ShoppingCartDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_TABLE_PRODUCT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL(DROP_TABLE_SHOPPING_CART);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void addOrderItem(OrderItem orderItem) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_NAME, orderItem.getProduct().getName());
        values.put(COLUMN_NAME_DESCRIPTION, orderItem.getProduct().getDescription());
        values.put(COLUMN_NAME_UNIT, orderItem.getProduct().getUnit());
        values.put(COLUMN_NAME_PRICE, orderItem.getProduct().getPrice());
        values.put(COLUMN_NAME_IMAGE_URL, orderItem.getProduct().getImage());
        values.put(COLUMN_NAME_STATUS, Constant.PENDING);
        values.put(COLUMN_NAME_QUANTITY, orderItem.getQuantity());
        values.put(COLUMN_NAME_PRODUCT_ID, orderItem.getProduct().getId());
        long newRowId = db.insert(TABLE_NAME_SHOPPING_CART, null, values);
        orderItem.setId(newRowId);
    }

    public OrderItem getOrderItem(long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String[] columnsToFetch = {
                COLUMN_NAME_ID,
                COLUMN_NAME_NAME,
                COLUMN_NAME_DESCRIPTION,
                COLUMN_NAME_UNIT,
                COLUMN_NAME_PRICE,
                COLUMN_NAME_IMAGE_URL,
                COLUMN_NAME_PRODUCT_ID,
                COLUMN_NAME_STATUS,
                COLUMN_NAME_QUANTITY
        };
        String[] columnWhereClauseValueById = {
                String.valueOf(id)
        };

        Cursor cursor = db.query(TABLE_NAME_SHOPPING_CART, columnsToFetch, columnWhereClauseById,
                columnWhereClauseValueById, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        String name = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_NAME));
        String description = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_DESCRIPTION));
        String unit = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_UNIT));
        double price = cursor.getDouble(cursor.getColumnIndexOrThrow(COLUMN_NAME_PRICE));
        String image = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_IMAGE_URL));
        int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_NAME_QUANTITY));
        String status = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_STATUS));
        long productId = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_NAME_PRODUCT_ID));
        Product product = new Product(productId, name, description, image, price, unit);
        OrderItem orderItem = new OrderItem(quantity, status, product);
        return orderItem;
    }

    public List<OrderItem> getAllOrderItem() {
        List<OrderItem> orderItemList = new ArrayList<OrderItem>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_SHOPPING_CART;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_NAME_ID));
                String name = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_NAME));
                String description = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_DESCRIPTION));
                String unit = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_UNIT));
                double price = cursor.getDouble(cursor.getColumnIndexOrThrow(COLUMN_NAME_PRICE));
                String image = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_IMAGE_URL));
                int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_NAME_QUANTITY));
                String status = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_STATUS));
                long productId = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_NAME_PRODUCT_ID));
                Product product = new Product(productId, name, description, image, price, unit);
                OrderItem orderItem = new OrderItem(quantity, status, product);
                orderItemList.add(orderItem);
            } while (cursor.moveToNext());
        }
        return orderItemList;
    }

    public int updateOrderItem(OrderItem orderItem) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_NAME, orderItem.getProduct().getName());
        values.put(COLUMN_NAME_DESCRIPTION, orderItem.getProduct().getDescription());
        values.put(COLUMN_NAME_UNIT, orderItem.getProduct().getUnit());
        values.put(COLUMN_NAME_PRICE, orderItem.getProduct().getPrice());
        values.put(COLUMN_NAME_IMAGE_URL, orderItem.getProduct().getImage());
        values.put(COLUMN_NAME_PRODUCT_ID, orderItem.getProduct().getId());
        values.put(COLUMN_NAME_STATUS, orderItem.getStatus());
        values.put(COLUMN_NAME_QUANTITY, orderItem.getQuantity());

        String[] columnWhereClauseValueById = {
                String.valueOf(orderItem.getId())
        };

        return db.update(TABLE_NAME_SHOPPING_CART, values, columnWhereClauseById,
                columnWhereClauseValueById);
    }

    public void deleteOrderItem(long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] columnWhereClauseValueById = {
                String.valueOf(id)
        };
        db.delete(TABLE_NAME_SHOPPING_CART, columnWhereClauseById, columnWhereClauseValueById);
        db.close();
    }

    public void deleteOrderItemByProductId(long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_SHOPPING_CART, String.format("WHERE %s = %s", COLUMN_NAME_PRODUCT_ID, id), null);
        db.close();
    }

}
