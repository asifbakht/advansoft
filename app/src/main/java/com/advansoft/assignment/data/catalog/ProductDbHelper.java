package com.advansoft.assignment.data.catalog;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.advansoft.assignment.data.catalog.model.Product;
import com.advansoft.assignment.data.catalog.model.ProductType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public class ProductDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "eCommerce";
    private static final int DATABASE_VERSION = 1;
    private Context context;

    /*
    TABLE name
     */

    private static final String TABLE_NAME_PRODUCT = "Product";

    /*
    TABLE columns
     */
    private static final String COLUMN_NAME_ID = "_id";
    private static final String COLUMN_NAME_NAME = "name";
    private static final String COLUMN_NAME_TYPE = "type";
    private static final String COLUMN_NAME_DESCRIPTION = "description";
    private static final String COLUMN_NAME_UNIT = "unit";
    private static final String COLUMN_NAME_PRICE = "price";
    private static final String COLUMN_NAME_IMAGE_URL = "image_url";


    private static final String CREATE_TABLE_PRODUCT = String.format("CREATE TABLE %s " +
                    " (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s TEXT, " +
                    "  %s TEXT, %s DOUBLE, %s TEXT);", TABLE_NAME_PRODUCT, COLUMN_NAME_ID,
            COLUMN_NAME_NAME, COLUMN_NAME_TYPE, COLUMN_NAME_DESCRIPTION, COLUMN_NAME_UNIT,
            COLUMN_NAME_PRICE, COLUMN_NAME_IMAGE_URL);

    private static final String DROP_TABLE_PRODUCT = String.format("DROP TABLE IF EXISTS %s", TABLE_NAME_PRODUCT);

    String columnWhereClauseById = COLUMN_NAME_ID + " = ? ";


    public ProductDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_TABLE_PRODUCT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            //db.execSQL(DROP_TABLE_PRODUCT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void addProduct(Product product, ProductType productType) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_NAME, product.getName());
        values.put(COLUMN_NAME_TYPE, productType.name);
        values.put(COLUMN_NAME_DESCRIPTION, product.getDescription());
        values.put(COLUMN_NAME_UNIT, product.getUnit());
        values.put(COLUMN_NAME_PRICE, product.getPrice());
        values.put(COLUMN_NAME_IMAGE_URL, product.getImage());
        long newRowId = db.insert(TABLE_NAME_PRODUCT, null, values);
        product.setId(newRowId);
    }

    public Product getProduct(long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String[] columnsToFetch = {
                COLUMN_NAME_ID,
                COLUMN_NAME_NAME,
                COLUMN_NAME_TYPE,
                COLUMN_NAME_DESCRIPTION,
                COLUMN_NAME_UNIT,
                COLUMN_NAME_PRICE,
                COLUMN_NAME_IMAGE_URL
        };
        String[] columnWhereClauseValueById = {
                String.valueOf(id)
        };

        Cursor cursor = db.query(TABLE_NAME_PRODUCT, columnsToFetch, columnWhereClauseById,
                columnWhereClauseValueById, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        String name = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_NAME));
        String type = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_TYPE));
        String description = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_DESCRIPTION));
        String unit = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_UNIT));
        double price = cursor.getDouble(cursor.getColumnIndexOrThrow(COLUMN_NAME_PRICE));
        String image = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_IMAGE_URL));
        Product product = new Product(id, name, description, image, price, unit);
        return product;
    }

    public List<Product> getAllProducts() {
        List<Product> productList = new ArrayList<Product>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_PRODUCT;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_NAME_ID));
                String name = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_NAME));
                String type = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_TYPE));
                String description = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_DESCRIPTION));
                String unit = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_UNIT));
                double price = cursor.getDouble(cursor.getColumnIndexOrThrow(COLUMN_NAME_PRICE));
                String image = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_IMAGE_URL));
                Product product = new Product(id, name, description, image, price, unit);
                productList.add(product);
            } while (cursor.moveToNext());
        }
        return productList;
    }

    public int updateProduct(Product product, ProductType productType) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_NAME, product.getName());
        values.put(COLUMN_NAME_TYPE, productType.name);
        values.put(COLUMN_NAME_DESCRIPTION, product.getDescription());
        values.put(COLUMN_NAME_UNIT, product.getUnit());
        values.put(COLUMN_NAME_PRICE, product.getPrice());
        values.put(COLUMN_NAME_IMAGE_URL, product.getImage());

        String[] columnWhereClauseValueById = {
                String.valueOf(product.getId())
        };

        return db.update(TABLE_NAME_PRODUCT, values, columnWhereClauseById,
                columnWhereClauseValueById);
    }

    public void deleteProduct(Product product) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] columnWhereClauseValueById = {
                String.valueOf(product.getId())
        };
        db.delete(TABLE_NAME_PRODUCT, columnWhereClauseById, columnWhereClauseValueById);
        db.close();
    }

    public List<Product> getAllProductsProductByType(ProductType productType) {
        List<Product> productList = new ArrayList<Product>();
        String selectQuery = String.format("SELECT * FROM %s WHERE %s = ?", TABLE_NAME_PRODUCT, COLUMN_NAME_TYPE);
        String[] columnWhereClauseValueByType = {
                productType.name
        };
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, columnWhereClauseValueByType);
        if (cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_NAME_ID));
                String name = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_NAME));
                String type = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_TYPE));
                String description = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_DESCRIPTION));
                String unit = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_UNIT));
                double price = cursor.getDouble(cursor.getColumnIndexOrThrow(COLUMN_NAME_PRICE));
                String image = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_IMAGE_URL));
                Product product = new Product(id, name, description, image, price, unit);
                productList.add(product);
            } while (cursor.moveToNext());
        }
        return productList;
    }
}
