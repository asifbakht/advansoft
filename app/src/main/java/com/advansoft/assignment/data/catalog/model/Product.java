package com.advansoft.assignment.data.catalog.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public class Product implements Parcelable {

    private long id;
    private String name;
    private String description;
    private String image;
    private double price;
    private String unit;

    public Product() {
        this.id = 0;
        this.name = "";
        this.description = "";
        this.image = "";
        this.price = 0d;
        this.unit = "";
    }

    public Product(long id, String name, String description, String image, double price, String unit) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.image = image;
        this.price = price;
        this.unit = unit;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.image);
        dest.writeDouble(this.price);
        dest.writeString(this.unit);
    }

    protected Product(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.description = in.readString();
        this.image = in.readString();
        this.price = in.readDouble();
        this.unit = in.readString();
    }

    public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel source) {
            return new Product(source);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
}
