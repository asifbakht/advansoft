package com.advansoft.assignment.data.catalog.source.local;

import com.advansoft.assignment.data.catalog.ProductDbHelper;
import com.advansoft.assignment.data.catalog.DataSourceProduct;
import com.advansoft.assignment.data.catalog.model.Product;
import com.advansoft.assignment.data.catalog.model.ProductType;

import java.util.List;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public class LocalDataSourceProduct implements DataSourceProduct {

    private ProductDbHelper productDbHelper;
    private static LocalDataSourceProduct instance;

    public LocalDataSourceProduct(ProductDbHelper productDbHelper) {
        this.productDbHelper = productDbHelper;
    }


    public static LocalDataSourceProduct getInstance(ProductDbHelper productDbHelper) {
        if (instance == null) {
            synchronized (LocalDataSourceProduct.class) {
                if (instance == null) {
                    instance = new LocalDataSourceProduct(productDbHelper);
                }
            }
        }
        return instance;
    }

    @Override
    public void loadProductList(LoadProductListCallBack callBack) {

    }

    @Override
    public void loadProduct(long id, LoadProductCallBack callBack) {

    }

    @Override
    public void addProduct(Product product, ProductType productType) {
        productDbHelper.addProduct(product, productType);
    }

    @Override
    public List<Product> getProductList() {
        List<Product> productList = productDbHelper.getAllProducts();
        return productList;
    }

    @Override
    public Product getProduct(long id) {
        Product product = productDbHelper.getProduct(id);
        return product;
    }

    @Override
    public List<Product> getProductListByProductType(ProductType type) {
        List<Product> productList = productDbHelper.getAllProductsProductByType(type);
        return productList;
    }
}
