package com.advansoft.assignment.data.shoppingcart.source.remote;

import com.advansoft.assignment.data.shoppingcart.model.OrderItem;
import com.advansoft.assignment.data.shoppingcart.source.DataSourceShoppingCart;

import java.util.List;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public class RemoteDataSourceShoppingCart implements DataSourceShoppingCart {

    private static RemoteDataSourceShoppingCart instance;

    public RemoteDataSourceShoppingCart() {
    }

    public static RemoteDataSourceShoppingCart getInstance() {
        if (instance == null) {
            synchronized (RemoteDataSourceShoppingCart.class) {
                if (instance == null) {
                    instance = new RemoteDataSourceShoppingCart();
                }
            }
        }
        return instance;
    }

    @Override
    public void addOrderItem(OrderItem orderItem) {

    }

    @Override
    public List<OrderItem> getOrderItemList() {
        return null;
    }

    @Override
    public OrderItem getOrderItem(long id) {
        return null;
    }

    @Override
    public void deleteOrderItemByProductId(long id) {

    }

    @Override
    public void deleteOrderItem(long id) {

    }

    @Override
    public void updateOrderItem(OrderItem orderItem) {

    }
}
