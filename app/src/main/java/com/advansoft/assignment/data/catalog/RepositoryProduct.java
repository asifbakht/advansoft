package com.advansoft.assignment.data.catalog;

import android.support.annotation.NonNull;

import com.advansoft.assignment.data.catalog.model.Product;
import com.advansoft.assignment.data.catalog.model.ProductType;
import com.advansoft.assignment.data.catalog.source.local.LocalDataSourceProduct;
import com.advansoft.assignment.data.catalog.source.remote.RemoteDataSourceProduct;

import java.util.List;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public class RepositoryProduct implements DataSourceProduct {

    LocalDataSourceProduct localDataSourceProduct;
    RemoteDataSourceProduct remoteDataSourceProduct;

    private static RepositoryProduct instance;

    public RepositoryProduct(LocalDataSourceProduct localSource, RemoteDataSourceProduct remoteSource) {
        this.localDataSourceProduct = localSource;
        this.remoteDataSourceProduct = remoteSource;
    }

    public static RepositoryProduct getInstance(@NonNull LocalDataSourceProduct localSource, @NonNull RemoteDataSourceProduct remoteSource) {
        if (instance == null) {
            synchronized (RepositoryProduct.class) {
                if (instance == null) {
                    instance = new RepositoryProduct(localSource, remoteSource);
                }
            }
        }
        return instance;
    }


    @Override
    public void loadProductList(LoadProductListCallBack callBack) {
        //NETWORK code
    }

    @Override
    public void loadProduct(long id, LoadProductCallBack callBack) {
        //NETWORK code
    }

    @Override
    public void addProduct(Product product, ProductType productType) {
        localDataSourceProduct.addProduct(product, productType);
    }

    @Override
    public List<Product> getProductList() {
        return localDataSourceProduct.getProductList();
    }

    @Override
    public Product getProduct(long id) {
        return localDataSourceProduct.getProduct(id);
    }

    @Override
    public List<Product> getProductListByProductType(ProductType type) {
        return localDataSourceProduct.getProductListByProductType(type);
    }
}
