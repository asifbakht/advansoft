package com.advansoft.assignment.data.shoppingcart.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.advansoft.assignment.data.catalog.model.Product;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public class OrderItem implements Parcelable {
    private long id;
    private Product product;
    private int quantity;
    private String status;

    public OrderItem(int quantity, String status, Product product) {
        this.product = product;
        this.quantity = quantity;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public void setProduct(Product product) {
        this.product = product;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public int getQuantity() {
        return quantity;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeParcelable(this.product, flags);
        dest.writeInt(this.quantity);
    }

    public OrderItem() {
    }

    protected OrderItem(Parcel in) {
        this.id = in.readInt();
        this.product = in.readParcelable(Product.class.getClassLoader());
        this.quantity = in.readInt();
    }

    public static final Parcelable.Creator<OrderItem> CREATOR = new Parcelable.Creator<OrderItem>() {
        @Override
        public OrderItem createFromParcel(Parcel source) {
            return new OrderItem(source);
        }

        @Override
        public OrderItem[] newArray(int size) {
            return new OrderItem[size];
        }
    };
}
