package com.advansoft.assignment.data.catalog;

import com.advansoft.assignment.data.catalog.model.Product;
import com.advansoft.assignment.data.catalog.model.ProductType;

import java.util.List;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public interface DataSourceProduct {

    interface LoadProductListCallBack {
        public void onProductListLoaded(List<Product> productList);

        public void onDataUnavailable(String message);
    }

    interface LoadProductCallBack {
        public void onProductLoaded(Product contact);

        public void onDataUnavailable(String message);
    }

    public void loadProductList(LoadProductListCallBack callBack);

    public void loadProduct(long id, LoadProductCallBack callBack);

    public void addProduct(Product product, ProductType productType);

    public List<Product> getProductList();

    public Product getProduct(long id);

    public List<Product> getProductListByProductType(ProductType type);
}
