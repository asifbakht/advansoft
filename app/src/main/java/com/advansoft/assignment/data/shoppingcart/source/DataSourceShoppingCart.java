package com.advansoft.assignment.data.shoppingcart.source;

import com.advansoft.assignment.data.shoppingcart.model.OrderItem;

import java.util.List;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public interface DataSourceShoppingCart {


    public void addOrderItem(OrderItem orderItem);

    public List<OrderItem> getOrderItemList();

    public OrderItem getOrderItem(long id);

    public void deleteOrderItemByProductId(long id);

    public void deleteOrderItem(long id);

    public void updateOrderItem(OrderItem orderItem);

}
