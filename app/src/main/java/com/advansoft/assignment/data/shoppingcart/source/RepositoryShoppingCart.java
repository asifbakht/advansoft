package com.advansoft.assignment.data.shoppingcart.source;

import android.support.annotation.NonNull;

import com.advansoft.assignment.data.shoppingcart.model.OrderItem;
import com.advansoft.assignment.data.shoppingcart.source.local.LocalDataSourceShoppingCart;
import com.advansoft.assignment.data.shoppingcart.source.remote.RemoteDataSourceShoppingCart;

import java.util.List;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public class RepositoryShoppingCart implements DataSourceShoppingCart {

    LocalDataSourceShoppingCart localDataSourceProduct;
    RemoteDataSourceShoppingCart remoteDataSourceShoppingCart;

    private static RepositoryShoppingCart instance;

    public RepositoryShoppingCart(LocalDataSourceShoppingCart localDataSourceProduct, RemoteDataSourceShoppingCart remoteDataSourceShoppingCart) {
        this.localDataSourceProduct = localDataSourceProduct;
        this.remoteDataSourceShoppingCart = remoteDataSourceShoppingCart;
    }

    public static RepositoryShoppingCart getInstance(@NonNull LocalDataSourceShoppingCart localSource, @NonNull RemoteDataSourceShoppingCart remoteSource) {
        if (instance == null) {
            synchronized (RepositoryShoppingCart.class) {
                if (instance == null) {
                    instance = new RepositoryShoppingCart(localSource, remoteSource);
                }
            }
        }
        return instance;
    }


    @Override
    public void addOrderItem(OrderItem orderItem) {
        localDataSourceProduct.addOrderItem(orderItem);
    }

    @Override
    public List<OrderItem> getOrderItemList() {
        return localDataSourceProduct.getOrderItemList();
    }

    @Override
    public OrderItem getOrderItem(long id) {
        return localDataSourceProduct.getOrderItem(id);
    }

    @Override
    public void deleteOrderItem(long id) {
        localDataSourceProduct.deleteOrderItem(id);
    }

    @Override
    public void deleteOrderItemByProductId(long id) {
        localDataSourceProduct.deleteOrderItemByProductId(id);
    }

    @Override
    public void updateOrderItem(OrderItem orderItem) {
        localDataSourceProduct.updateOrderItem(orderItem);
    }
}
