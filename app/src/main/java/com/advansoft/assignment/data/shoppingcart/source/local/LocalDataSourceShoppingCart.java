package com.advansoft.assignment.data.shoppingcart.source.local;

import com.advansoft.assignment.data.shoppingcart.model.OrderItem;
import com.advansoft.assignment.data.shoppingcart.source.DataSourceShoppingCart;
import com.advansoft.assignment.data.shoppingcart.source.ShoppingCartDbHelper;

import java.util.List;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public class LocalDataSourceShoppingCart implements DataSourceShoppingCart {

    private ShoppingCartDbHelper shoppingCartDbHelper;
    private static LocalDataSourceShoppingCart instance;

    public LocalDataSourceShoppingCart(ShoppingCartDbHelper shoppingCartDbHelper) {
        this.shoppingCartDbHelper = shoppingCartDbHelper;
    }

    public static LocalDataSourceShoppingCart getInstance(ShoppingCartDbHelper shoppingCartDbHelper) {
        if (instance == null) {
            synchronized (LocalDataSourceShoppingCart.class) {
                if (instance == null) {
                    instance = new LocalDataSourceShoppingCart(shoppingCartDbHelper);
                }
            }
        }
        return instance;
    }

    @Override
    public void addOrderItem(OrderItem orderItem) {
        shoppingCartDbHelper.addOrderItem(orderItem);
    }

    @Override
    public List<OrderItem> getOrderItemList() {
        return shoppingCartDbHelper.getAllOrderItem();
    }

    @Override
    public OrderItem getOrderItem(long id) {
        return shoppingCartDbHelper.getOrderItem(id);
    }

    @Override
    public void deleteOrderItemByProductId(long id) {
        shoppingCartDbHelper.deleteOrderItemByProductId(id);
    }

    @Override
    public void deleteOrderItem(long id) {
        shoppingCartDbHelper.deleteOrderItem(id);
    }

    @Override
    public void updateOrderItem(OrderItem orderItem) {
        shoppingCartDbHelper.updateOrderItem(orderItem);
    }
}