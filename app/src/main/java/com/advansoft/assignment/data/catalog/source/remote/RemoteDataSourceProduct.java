package com.advansoft.assignment.data.catalog.source.remote;

import com.advansoft.assignment.data.catalog.DataSourceProduct;
import com.advansoft.assignment.data.catalog.model.Product;
import com.advansoft.assignment.data.catalog.model.ProductType;

import java.util.List;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public class RemoteDataSourceProduct implements DataSourceProduct {

    private static RemoteDataSourceProduct instance;

    public RemoteDataSourceProduct() {
    }


    public static RemoteDataSourceProduct getInstance() {
        if (instance == null) {
            synchronized (RemoteDataSourceProduct.class) {
                if (instance == null) {
                    instance = new RemoteDataSourceProduct();
                }
            }
        }
        return instance;
    }

    @Override
    public void loadProductList(LoadProductListCallBack callBack) {

    }

    @Override
    public void loadProduct(long id, LoadProductCallBack callBack) {

    }

    @Override
    public void addProduct(Product product, ProductType productType) {

    }


    @Override
    public List<Product> getProductList() {
        return null;
    }

    @Override
    public Product getProduct(long id) {
        return null;
    }

    @Override
    public List<Product> getProductListByProductType(ProductType type) {
        return null;
    }
}
