package com.advansoft.assignment.ui.catalog.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.advansoft.assignment.R;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public class CatalogHeaderViewHolder extends RecyclerView.ViewHolder {
    TextView textViewProductType;
    View rootView;
    public CatalogHeaderViewHolder(View headerView) {
        super(headerView);
        textViewProductType = (TextView) headerView.findViewById(R.id.textView_ProductType);
        rootView = headerView;
    }

    public TextView getTextViewProductType(){
        return textViewProductType;
    }
    public View getRootView(){
        return rootView;
    }
}
