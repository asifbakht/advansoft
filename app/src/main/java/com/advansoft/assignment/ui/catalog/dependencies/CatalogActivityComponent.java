package com.advansoft.assignment.ui.catalog.dependencies;

import com.advansoft.assignment.dagger.ActivityScope;
import com.advansoft.assignment.dagger.AppComponents;
import com.advansoft.assignment.data.catalog.RepositoryProduct;
import com.advansoft.assignment.data.shoppingcart.source.RepositoryShoppingCart;
import com.advansoft.assignment.ui.catalog.activity.CatalogActivity;

import dagger.Component;

/**
 * Created by Asif Bakht on 11/9/2017.
 */

@ActivityScope
@Component(modules = CatalogActivityModule.class, dependencies = AppComponents.class)
public interface CatalogActivityComponent {
    void inject(CatalogActivity catalogActivity);

    RepositoryProduct getRepositoryProduct();

    RepositoryShoppingCart getRepositoryShoppingCart();
}