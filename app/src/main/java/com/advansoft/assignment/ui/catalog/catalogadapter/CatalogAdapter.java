package com.advansoft.assignment.ui.catalog.catalogadapter;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import com.advansoft.assignment.R;
import com.advansoft.assignment.data.catalog.model.Product;
import com.advansoft.assignment.data.catalog.model.ProductType;
import com.advansoft.assignment.ui.catalog.viewholders.CatalogHeaderViewHolder;
import com.advansoft.assignment.ui.catalog.viewholders.CatalogItemViewHolder;

import java.util.List;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public class CatalogAdapter extends StatelessSection {

    public interface Listener {
        void onChecked(int position, int quantity, Product product);

        void onUnchecked(long productId);
    }

    Listener listener;

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public Listener getListener() {
        return listener;
    }


    private final List<Product> productList;
    private final ProductType productType;
    private boolean expanded = true;

    public CatalogAdapter(ProductType productType, List<Product> productList) {
        super(new SectionParameters.Builder(R.layout.layout_catalog_item)
                .headerResourceId(R.layout.layout_catalog_header)
                .build());
        this.productList = productList;
        this.productType = productType;
    }


    @Override
    public int getContentItemsTotal() {
        return productList.size();
    }

    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        return new CatalogItemViewHolder(view);
    }

    @Override
    public void onBindItemViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final CatalogItemViewHolder catalogItemViewHolder = (CatalogItemViewHolder) holder;

        final Product product = productList.get(position);
        String name = product.getName();
        double price = product.getPrice();
        String image = product.getImage();
        String unit = product.getUnit();
        catalogItemViewHolder.getTextViewProductName().setText(name);
        catalogItemViewHolder.getImageViewProduct().setImageURI(null);
        catalogItemViewHolder.getTextViewProductUnit().setText(unit);
        catalogItemViewHolder.getTextViewProductPrice().setText("" + price);
        catalogItemViewHolder.getCheckBoxProductSelection().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = ((CheckBox) v).isChecked();
                Editable editable = catalogItemViewHolder.getProductQuantity().getText();
                if (editable != null) {
                    if (isChecked) {
                        int quantity = Integer.valueOf(editable.toString());
                        listener.onChecked(position, quantity, product);
                    } else {
                        listener.onUnchecked(product.getId());
                    }
                }
            }
        });
        catalogItemViewHolder.getRootView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(catalogItemViewHolder.getRootView().getContext(), String.format("Clicked on position #%s of Section %s",
                        "Test", productType.name),
                        Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
        return new CatalogHeaderViewHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
        final CatalogHeaderViewHolder headerHolder = (CatalogHeaderViewHolder) holder;

        headerHolder.getTextViewProductType().setText(productType.name);
        headerHolder.getRootView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expanded = !expanded;
            }
        });
    }
}
