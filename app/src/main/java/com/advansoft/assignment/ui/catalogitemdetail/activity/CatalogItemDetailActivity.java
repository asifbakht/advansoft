package com.advansoft.assignment.ui.catalogitemdetail.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.advansoft.assignment.R;

public class CatalogItemDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalog_item_detail);
    }
}
