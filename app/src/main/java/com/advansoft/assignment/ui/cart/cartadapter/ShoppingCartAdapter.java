package com.advansoft.assignment.ui.cart.cartadapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.advansoft.assignment.R;
import com.advansoft.assignment.data.catalog.model.Product;
import com.advansoft.assignment.data.shoppingcart.model.OrderItem;
import com.advansoft.assignment.data.shoppingcart.model.ShoppingCart;
import com.advansoft.assignment.ui.cart.viewholder.ShoppingCartItemViewHolder;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public class ShoppingCartAdapter extends RecyclerView.Adapter<ShoppingCartItemViewHolder> {

    private ShoppingCart shoppingCart;

    public ShoppingCartAdapter(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    @Override
    public ShoppingCartItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_shopping_cart_cardview, parent, false);
        return new ShoppingCartItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ShoppingCartItemViewHolder holder, int position) {
        OrderItem item = shoppingCart.getItems().get(position);
        Product product = item.getProduct();
        holder.productUnit.setText(product.getUnit());
        holder.productQuantity.setText(String.valueOf(item.getQuantity()));
        holder.productPrice.setText(String.valueOf(product.getPrice()));
        holder.productName.setText(product.getName());
        holder.productImage.setImageURI(null);
    }

    @Override
    public int getItemCount() {
        return shoppingCart.getItemSize();
    }
}
