package com.advansoft.assignment.ui.catalog.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.advansoft.assignment.R;
import com.advansoft.assignment.dagger.Advantsoft;
import com.advansoft.assignment.data.catalog.RepositoryProduct;
import com.advansoft.assignment.data.catalog.model.Product;
import com.advansoft.assignment.data.catalog.model.ProductType;
import com.advansoft.assignment.data.shoppingcart.model.OrderItem;
import com.advansoft.assignment.data.shoppingcart.source.RepositoryShoppingCart;
import com.advansoft.assignment.ui.catalog.catalogadapter.CatalogAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;

public class CatalogActivity extends AppCompatActivity implements CatalogAdapter.Listener {

    private SectionedRecyclerViewAdapter catalogAdapter;
    Button buttonComplete;
    Map<ProductType, List<Product>> productList;

    @Inject
    RepositoryProduct repositoryProduct;

    @Inject
    RepositoryShoppingCart repositoryShoppingCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        ((Advantsoft) getApplication()).getAppComponents().inject(this);
        catalogAdapter = new SectionedRecyclerViewAdapter();
        productList = new HashMap<>();
        productList.put(ProductType.DAIRY, repositoryProduct.getProductListByProductType(ProductType.DAIRY));
        productList.put(ProductType.COOKING, repositoryProduct.getProductListByProductType(ProductType.COOKING));
        productList.put(ProductType.GARMENTS, repositoryProduct.getProductListByProductType(ProductType.GARMENTS));

        for (Object o : productList.entrySet()) {
            Map.Entry pair = (Map.Entry) o;
            ProductType type = (ProductType) pair.getKey();
            ArrayList productList = (ArrayList) pair.getValue();
            CatalogAdapter garmentSection = new CatalogAdapter(type, productList);
            garmentSection.setListener(this);
            catalogAdapter.addSection(garmentSection);
        }


//        Product dairyA = new Product(1, "Yogurt", "N/A", null, 25.0, "kg");
//        Product dairyB = new Product(2, "Milk", "N/A", null, 10.0, "litre");
//        Product dairyC = new Product(3, "Cream", "N/A", null, 15.0, "kg");
//        Product cookingA = new Product(4, "Oil", "N/A", null, 45.0, "litre");
//        Product cookingB = new Product(5, "Ghee", "N/A", null, 60.0, "litre");
//        Product garmentA = new Product(6, "Shirt", "N/A", null, 125.0, "size");
//        Product garmentB = new Product(7, "Trouser", "N/A", null, 150.0, "size");
//
//        repositoryProduct.addProduct(dairyA, ProductType.DAIRY);
//        repositoryProduct.addProduct(dairyB, ProductType.DAIRY);
//        repositoryProduct.addProduct(dairyC, ProductType.DAIRY);
//        repositoryProduct.addProduct(cookingA, ProductType.COOKING);
//        repositoryProduct.addProduct(cookingB, ProductType.COOKING);
//        repositoryProduct.addProduct(garmentA, ProductType.GARMENTS);
//        repositoryProduct.addProduct(garmentB, ProductType.GARMENTS);

        RecyclerView recyclerView = findViewById(R.id.recycler_catalogList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(catalogAdapter);
        catalogAdapter.notifyDataSetChanged();

        buttonComplete = findViewById(R.id.button_Complete);

        buttonComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });


    }

    @Override
    public void onChecked(int position, int quantity, Product product) {
        repositoryShoppingCart.addOrderItem(new OrderItem(quantity, null, product));
    }

    @Override
    public void onUnchecked(long productId) {
        repositoryShoppingCart.deleteOrderItemByProductId(productId);
    }
}
