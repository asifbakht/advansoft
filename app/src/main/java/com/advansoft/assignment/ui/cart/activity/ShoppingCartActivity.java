package com.advansoft.assignment.ui.cart.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.advansoft.assignment.R;
import com.advansoft.assignment.dagger.Advantsoft;
import com.advansoft.assignment.data.shoppingcart.model.ShoppingCart;
import com.advansoft.assignment.data.shoppingcart.source.RepositoryShoppingCart;
import com.advansoft.assignment.ui.cart.cartadapter.ShoppingCartAdapter;
import com.advansoft.assignment.ui.catalog.activity.CatalogActivity;

import javax.inject.Inject;

public class ShoppingCartActivity extends AppCompatActivity {


    private ShoppingCart shoppingCart;

    Button buttonAddItem;
    TextView textViewNoData;
    RecyclerView recyclerViewCart;

    ShoppingCartAdapter shoppingCartAdapter;
    @Inject
    RepositoryShoppingCart repositoryShoppingCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        ((Advantsoft) getApplication()).getAppComponents().inject(this);

        buttonAddItem = findViewById(R.id.button_Add);
        buttonAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent p = new Intent(ShoppingCartActivity.this, CatalogActivity.class);
                startActivityForResult(p, 1);
            }
        });
        shoppingCart = new ShoppingCart();
        shoppingCart.getItems().addAll(repositoryShoppingCart.getOrderItemList());
        recyclerViewCart = findViewById(R.id.recycler_cartList);
        shoppingCartAdapter = new ShoppingCartAdapter(shoppingCart);
        recyclerViewCart.setAdapter(shoppingCartAdapter);
        textViewNoData = findViewById(R.id.textView_NoData);
        shoppingCartAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                shoppingCart = new ShoppingCart();
                shoppingCart.getItems().addAll(repositoryShoppingCart.getOrderItemList());
                if (shoppingCart.getItemSize() > 0) {
                    textViewNoData.setVisibility(View.GONE);
                    recyclerViewCart.setVisibility(View.VISIBLE);
                    shoppingCartAdapter.notifyDataSetChanged();
                } else {
                    textViewNoData.setVisibility(View.VISIBLE);
                    recyclerViewCart.setVisibility(View.GONE);
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
            }
        }
    }

}
