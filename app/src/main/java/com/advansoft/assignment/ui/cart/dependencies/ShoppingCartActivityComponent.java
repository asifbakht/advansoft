package com.advansoft.assignment.ui.cart.dependencies;

import com.advansoft.assignment.dagger.ActivityScope;
import com.advansoft.assignment.dagger.AppComponents;
import com.advansoft.assignment.data.catalog.RepositoryProduct;
import com.advansoft.assignment.data.shoppingcart.source.RepositoryShoppingCart;
import com.advansoft.assignment.ui.cart.activity.ShoppingCartActivity;

import dagger.Component;

/**
 * Created by Asif Bakht on 11/9/2017.
 */

@ActivityScope
@Component(dependencies = AppComponents.class)
public interface ShoppingCartActivityComponent {
    void inject(ShoppingCartActivity shoppingCartActivity);

    RepositoryProduct getRepositoryProduct();

    RepositoryShoppingCart getRepositoryShoppingCart();
}