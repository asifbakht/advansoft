package com.advansoft.assignment.ui.cart.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.advansoft.assignment.R;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public class ShoppingCartItemViewHolder extends RecyclerView.ViewHolder {

    public TextView productName;
    public ImageView productImage;
    public TextView productPrice;
    public TextView productUnit;
    public EditText productQuantity;

    public ShoppingCartItemViewHolder(View itemView) {
        super(itemView);
        productImage = itemView.findViewById(R.id.imageView_ProductImage);
        productName = itemView.findViewById(R.id.textView_ProductName);
        productPrice = itemView.findViewById(R.id.textview_ProductPrice);
        productUnit = itemView.findViewById(R.id.textview_ProductUnit);
        productQuantity = itemView.findViewById(R.id.editText_ProductPrice);
    }
}
