package com.advansoft.assignment.ui.catalog.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.advansoft.assignment.R;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public class CatalogItemViewHolder extends RecyclerView.ViewHolder {
    private final ImageView imageViewProduct;
    private final TextView textViewProductName;
    private final TextView textViewProductUnit;
    private final TextView textViewProductPrice;
    private final EditText productQuantity;
    private final CheckBox checkBoxProductSelection;
    private final View rootView;

    public CatalogItemViewHolder(View itemView) {
        super(itemView);
        imageViewProduct = itemView.findViewById(R.id.imageView_ProductImage);
        textViewProductName = itemView.findViewById(R.id.textView_ProductName);
        productQuantity = itemView.findViewById(R.id.editText_ProductQuantity);
        textViewProductUnit = itemView.findViewById(R.id.textview_ProductUnit);
        textViewProductPrice = itemView.findViewById(R.id.textview_ProductPrice);
        checkBoxProductSelection = itemView.findViewById(R.id.checkbox_ProductSelect);
        rootView = itemView;
    }

    public ImageView getImageViewProduct() {
        return imageViewProduct;
    }

    public TextView getTextViewProductName() {
        return textViewProductName;
    }

    public EditText getProductQuantity() {
        return productQuantity;
    }

    public View getRootView() {
        return rootView;
    }

    public TextView getTextViewProductUnit() {
        return textViewProductUnit;
    }

    public TextView getTextViewProductPrice() {
        return textViewProductPrice;
    }

    public CheckBox getCheckBoxProductSelection() {
        return checkBoxProductSelection;
    }
}
