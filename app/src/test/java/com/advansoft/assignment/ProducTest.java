package com.advansoft.assignment;

import com.advansoft.assignment.data.catalog.model.Product;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asif Bakht on 11/8/2017.
 */

public class ProducTest {

    private static final String TABLE_NAME_PRODUCT = "Product";
    private static final String COLUMN_NAME_ID = "_id";
    private static final String COLUMN_NAME_NAME = "name";
    private static final String COLUMN_NAME_TYPE = "type";
    private static final String COLUMN_NAME_DESCRIPTION = "description";
    private static final String COLUMN_NAME_UNIT = "unit";
    private static final String COLUMN_NAME_PRICE = "price";
    private static final String COLUMN_NAME_IMAGE_URL = "image_url";


    private static final String CREATE_TABLE_PRODUCT = String.format("CREATE TABLE %s " +
                    " (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s TEXT, " +
                    "  %s TEXT, %s DOUBLE, %s TEXT);", TABLE_NAME_PRODUCT, COLUMN_NAME_ID,
            COLUMN_NAME_NAME, COLUMN_NAME_TYPE, COLUMN_NAME_DESCRIPTION, COLUMN_NAME_UNIT,
            COLUMN_NAME_PRICE, COLUMN_NAME_IMAGE_URL);

    private List<Product> systemUnderTest;

    @Before
    public void setup() {
        this.systemUnderTest = new ArrayList<Product>();
    }

    @After
    public void tearDown() {
        this.systemUnderTest = null;
    }

    @Test
    public void testGetFirstQuestion() {
        System.out.print(CREATE_TABLE_PRODUCT);
    }
}
